package com.example.wallofstreetv2.UserAdapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.wallofstreetv2.OnClickListener
import com.example.wallofstreetv2.R
import com.example.wallofstreetv2.Wall
import com.example.wallofstreetv2.databinding.ItemWallBinding
import com.google.firebase.storage.FirebaseStorage
import java.io.File


class WallAdapter(private var walls: List<Wall>, private val listener: OnClickListener) :
    RecyclerView.Adapter<WallAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemWallBinding.bind(view)
        fun setListener(wall: Wall) {
            binding.root.setOnClickListener {
                listener.onClick(wall)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_wall, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val wall = walls[position]
        with(holder) {
            setListener(wall)
            binding.nameWall.text = wall.name
            binding.artista.text = wall.artista
//            binding.latitudWall.text = wall.latitud.toString()
//            binding.longitudWall.text = wall.longitud.toString()
            val storage = FirebaseStorage.getInstance().reference.child(wall.url)
            val localFile = File.createTempFile("temp", "jpeg")


            storage.getFile(localFile).addOnSuccessListener {
                val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                binding.imgWall.setImageBitmap(bitmap)
            }.addOnFailureListener{
                Toast.makeText(context, "Error downloading image!", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    override fun getItemCount(): Int {
        return walls.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun getWallList(wallList: List<Wall>) {
        walls = wallList
        notifyDataSetChanged()
    }
}
