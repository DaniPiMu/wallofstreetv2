package com.example.wallofstreetv2.Fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentResultListener
import androidx.navigation.fragment.findNavController
import com.airbnb.lottie.LottieAnimationView
import com.example.wallofstreetv2.R
import com.example.wallofstreetv2.Wall
import com.example.wallofstreetv2.databinding.FragmentDetailPointBinding
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.io.File


class DetailPointFragment : Fragment() {

    private lateinit var binding: FragmentDetailPointBinding
    private val db = FirebaseFirestore.getInstance()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentDetailPointBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val wall: Wall = arguments?.getParcelable("wall")!!
        super.onViewCreated(view, savedInstanceState)
        obtenerDatos()

        binding.botonUbicacion.setOnClickListener {
            var longitud = wall.longitud
            var latitud = wall.latitud

            //Coordenadas
            parentFragmentManager.setFragmentResult("longitudList", bundleOf("longitudList" to longitud))
            parentFragmentManager.setFragmentResult("latitudList", bundleOf("latitudList" to latitud))
            findNavController().navigate(R.id.action_detailPointFragment_to_mapFragment)
        }

        binding.editar.setOnClickListener {
            camposEditables()
            editarDatos()
        }
        binding.eliminar.setOnClickListener {
            //showAlert()--> no me funciona
            db.collection("walls").document(wall.name).delete()
        }

    }
    private fun editarDatos() {
        var like = false
        binding.doneImageView.setOnClickListener {
            eliminamosDocumentEditar()
            creacionDocument()
            animacion(binding.doneImageView, R.raw.done, like )
        }
    }

    private fun eliminamosDocumentEditar() {
        val wall: Wall = arguments?.getParcelable("wall")!!
        db.collection("walls").document(wall.name).delete()
        findNavController().navigate(R.id.action_detailPointFragment_to_recyclerViewFragment)
    }

    private fun showAlert() {
        val wall: Wall = arguments?.getParcelable("wall")!!
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Eliminar marcador")
        builder.setMessage("¿Estas seguro que quieres eliminar el marcador ?")
        builder.setPositiveButton("SI,ELIMINAR") { dialogInterface: DialogInterface, i: Int ->
        db.collection("walls").document(wall.name).delete()
            findNavController().navigate(R.id.action_detailPointFragment_to_recyclerViewFragment)
        }
        builder.setNegativeButton("CANCELAR") { dialogInterface: DialogInterface, i: Int -> }
        builder.show()
    }

    private fun creacionDocument() {
        val wall: Wall = arguments?.getParcelable("wall")!!
        val nuevoNombre = binding.editarNombre.text.toString()
        val nuevoArtista = binding.editarArtista.text.toString()

        db.collection("walls").document(binding.nombre.text.toString()).set(
            hashMapOf(
                "url" to wall.url,
                "nombre" to nuevoNombre,
                "artista" to nuevoArtista,
                "longitud" to wall.longitud,
                "latitud" to wall.latitud
            )
        )
    }

    private fun obtenerDatos() {
        val wall: Wall = arguments?.getParcelable("wall")!!
        binding.nombre.setText(wall.name)
        binding.artista.setText(wall.artista)
        binding.textoUbicacionLongitud.setText(wall.longitud.toString())
        binding.textoUbicacionLatitud.setText(wall.latitud.toString())

        bajarImagen()
    }

    private fun camposEditables() {
            binding.nombre.visibility = View.INVISIBLE
            binding.artista.visibility = View.INVISIBLE

            binding.editarNombre.visibility = View.VISIBLE
            binding.editarArtista.visibility = View.VISIBLE

            binding.editar.visibility = View.INVISIBLE
            binding.doneImageView.visibility = View.VISIBLE

    }

    private fun bajarImagen() {

        val storage = FirebaseStorage.getInstance().reference.child(oldimg())
        val localFile = File.createTempFile("temp", "jpeg")
        storage.getFile(localFile).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
            binding.imgGallery.setImageBitmap(bitmap)
        }.addOnFailureListener {
            Toast.makeText(requireContext(), "Error downloading image!", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun oldimg(): String {
        val wall: Wall = arguments?.getParcelable("wall")!!
        return wall.url
    }
}

private fun animacion(imageView: LottieAnimationView,
                      animation:Int
                      ,like:Boolean):Boolean {
    if (!like){
        imageView.setAnimation(animation)
        imageView.playAnimation()

    }else{
        imageView.setImageResource(R.drawable.donepng)
    }
    return !like
}
