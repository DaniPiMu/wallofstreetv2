package com.example.wallofstreetv2.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.wallofstreetv2.OnClickListener
import com.example.wallofstreetv2.UserAdapter.WallAdapter
import com.example.wallofstreetv2.Wall
import com.example.wallofstreetv2.databinding.FragmentRecyclerViewBinding
import com.google.firebase.firestore.*


class RecyclerViewFragment : Fragment(), OnClickListener {

    private lateinit var wallAdapater: WallAdapter
    private lateinit var binding: FragmentRecyclerViewBinding
    private val db = FirebaseFirestore.getInstance()
    private var wallList = mutableListOf<Wall>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRecyclerViewBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView(arrayListOf())
        wallList = arrayListOf<Wall>()
        eventChangeListener()

    }

    private fun setupRecyclerView(wallsList: List<Wall>) {
        wallAdapater = WallAdapter(wallsList, this)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, 2)
            adapter = wallAdapater
        }
    }

    private fun eventChangeListener() {
        db.collection("walls").addSnapshotListener(object : EventListener<QuerySnapshot> {
            override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                if (error != null) {
                    Log.e("Firestore error", error.message.toString())
                    return
                }
                for (dc: DocumentChange in value?.documentChanges!!) {
                    if (dc.type == DocumentChange.Type.ADDED) {
                        val name = dc.document.get("nombre") as String
                        val artista = dc.document.get("artista")
                        val fileName: String = name + "_" + artista
                        val url = dc.document.get("url")
                        val wall = Wall(
                            url as String,
                            dc.document.get("nombre") as String,
                            dc.document.get("artista") as String,
                            dc.document.get("latitud") as Double,
                            dc.document.get("longitud") as Double
                        )

                        wallList.add(wall)
                    }
                }
                wallAdapater.getWallList(wallList)
            }
        })
    }

    override fun onClick(wall: Wall) {
        val action = RecyclerViewFragmentDirections.actionRecyclerViewFragmentToDetailPointFragment(wall)
        findNavController().navigate(action)
    }

}