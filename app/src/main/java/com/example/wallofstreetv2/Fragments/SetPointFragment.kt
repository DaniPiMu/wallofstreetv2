package com.example.wallofstreetv2.Fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentResultListener
import androidx.navigation.fragment.findNavController
import com.example.wallofstreetv2.R
import com.example.wallofstreetv2.databinding.FragmentSetPointBinding
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage


class SetPointFragment : Fragment() {

    private val db = FirebaseFirestore.getInstance()

    private lateinit var binding: FragmentSetPointBinding
    var fotoFinish: String = ""
    var latitud = 0.0
    var longitud = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSetPointBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        super.onViewCreated(view, savedInstanceState)

        //ubicacion
        parentFragmentManager.setFragmentResultListener("ubicacion", this,
            FragmentResultListener { requestKey: String, result: Bundle ->
                val result = result.getString("ubicacion")
                println(result)
                if (result != null) {
                    binding.textoUbicacionLatitud.text = result
                }
            })
        //Coordenadas
        parentFragmentManager.setFragmentResultListener("longitud", this,
            FragmentResultListener { requestKey: String, result: Bundle ->
                longitud = result.getDouble("longitud")
                var longitudString = longitud.toString()
                println(longitud)

            })
        parentFragmentManager.setFragmentResultListener("latitud", this,
            FragmentResultListener { requestKey: String, result: Bundle ->
                latitud = result.getDouble("latitud")
                println(latitud)
            })

        binding.camara.setOnClickListener {
            findNavController().navigate(R.id.action_setPointFragment_to_cameraFragment)
            parentFragmentManager.setFragmentResultListener("foto", this,
                FragmentResultListener { requestKey: String, result: Bundle ->
                    fotoFinish = result.getString("foto").toString()
                    println(fotoFinish)

                    imageView = binding.imgGallery
                    imageView.setImageURI(Uri.parse(fotoFinish))
                })
        }
        binding.gallery.setOnClickListener {
            openGallery()
        }
        binding.aAdir.setOnClickListener {
            subirImagen()
            findNavController().navigate(R.id.action_setPointFragment_to_mapFragment)
        }
    }

    private fun subirImagen() {
        val name = binding.nombre.text.toString()
        val artist = binding.artista.text.toString()
        val fileName: String = name + "_" + artist
        //
        val storage = FirebaseStorage.getInstance().getReference("images/$fileName")
        storage.putFile(Uri.parse(fotoFinish)).addOnSuccessListener {
            Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
            db.collection("walls").document(binding.nombre.text.toString()).set(
                hashMapOf(
                    "url" to it.storage.path,
                    "nombre" to binding.nombre.text.toString(),
                    "artista" to binding.artista.text.toString(),
                    "longitud" to longitud,
                    "latitud" to latitud
                )
            )
        }
            .addOnFailureListener {
                Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
            }
    }

    //Abrir la galeria y poner imagen
    private val PICK_IMAGE = 100
    private fun openGallery() {
        val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(gallery, PICK_IMAGE)
    }

    private var imageUri: Uri? = null
    lateinit var imageView: ImageView
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            fotoFinish = data?.data.toString()
            imageView = binding.imgGallery
            imageView.setImageURI(Uri.parse(fotoFinish))
        }
    }
}
