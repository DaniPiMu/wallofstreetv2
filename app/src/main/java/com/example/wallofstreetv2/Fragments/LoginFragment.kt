package com.example.wallofstreetv2.Fragments

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.wallofstreetv2.MainActivity
import com.example.wallofstreetv2.R
import com.example.wallofstreetv2.databinding.FragmentLogin2Binding
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider

class LoginFragment : Fragment() {


    private val GOOGLE_SIGN_IN = 100
    private lateinit var binding: FragmentLogin2Binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLogin2Binding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iniciarSesion()
        iniciarSesionGoogle()
        registrarse()
    }

    private fun iniciarSesionGoogle() {

        binding.googleButtom.setOnClickListener {
            val googleConf = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(com.firebase.ui.auth.R.string.default_web_client_id))
                .requestEmail()
                .build()
            val googleClient = GoogleSignIn.getClient(requireContext(), googleConf)
            val signInIntent = googleClient.signInIntent
            googleClient.signOut()
            startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
        }
    }

    private fun registrarse() {
        binding.btnSignIn.setOnClickListener {
            val fm = parentFragmentManager
            val transaction = fm.beginTransaction()
            transaction.replace(R.id.fragmentContainerView1, SignInFragment())
            transaction.commit()
        }
    }

    private fun iniciarSesion() {
        binding.btnLogIn.setOnClickListener {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(
                binding.loginName.text.toString(),
                binding.password.text.toString()
            )
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        val emailLogged = it.result?.user?.email
                        Toast.makeText(this.context, emailLogged, Toast.LENGTH_LONG).show()

                        val intent = Intent(context, MainActivity::class.java)

                        startActivity(intent)
                    } else {
                        showAlert()
                    }
                }
        }
    }

    private fun showAlert() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Error")
        builder.setMessage("Se ha producido un error de inicio de sesion")
        builder.setPositiveButton("aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GOOGLE_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                    FirebaseAuth.getInstance().signInWithCredential(credential)
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                val emailLogged = it.result?.user?.email
                                Toast.makeText(this.context, emailLogged, Toast.LENGTH_LONG).show()

                                val intent = Intent(context, MainActivity::class.java)
                                startActivity(intent)
                            } else {
                                showAlert()
                            }
                        }
                }
            } catch (e: ApiException) {
                showAlert()
            }
        }
    }
}