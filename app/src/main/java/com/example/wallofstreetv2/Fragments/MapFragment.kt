package com.example.wallofstreetv2.Fragments

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentResultListener
import androidx.navigation.fragment.findNavController
import com.example.wallofstreetv2.R
import com.example.wallofstreetv2.Wall
import com.example.wallofstreetv2.databinding.FragmentMap2Binding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.*


class MapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var map: GoogleMap
    private val db = FirebaseFirestore.getInstance()
    private val markersList = mutableListOf<Wall>()

    //Mapa
    fun createMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(binding.map.id) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        loadMarkers()
        enableLocation()
        clickPosition(map)
        coordenadasRecycleView(map)
    }

    private fun loadMarkers() {

        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            val smallMarker = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.pepe),80,80,false)
            val smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker)
            for (i in markersList) {
                val coordinates = LatLng(i.latitud, i.longitud)
                map.addMarker(
                    MarkerOptions()
                        .position(coordinates)
                        .title(i.name)
                        .icon(smallMarkerIcon)
                )
            }
        }, 3000)
    }

    private fun coordenadasRecycleView(map: GoogleMap) {

        var latitud = 41.454142
        var longitud = 2.186057
        //Coordenadas
        parentFragmentManager.setFragmentResultListener("longitudList", this,
            FragmentResultListener { requestKey: String, result: Bundle ->
                longitud = result.getDouble("longitudList")
                println(longitud)

            })
        parentFragmentManager.setFragmentResultListener("latitudList", this,
            FragmentResultListener { requestKey: String, result: Bundle ->
                latitud = result.getDouble("latitudList")
                println(latitud)
            })

        val coordinates = LatLng(latitud, longitud)
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 18f))

    }

    //must not be null, pero me da null
    private fun posicionActual() {

        val loc = map.myLocation
        val latitud = loc.latitude
        val longitud = loc.longitude

        val coordinates = LatLng(latitud, longitud)
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 18f))
    }

    private fun clickPosition(googleMap: GoogleMap) {
        //cuando clicamos, se mueve el marcador
        googleMap.setOnMapClickListener { latLng -> // Creating a marker
            val markerOptions = MarkerOptions()

            // Setting the position for the marker
            markerOptions.position(latLng)

            // Setting the title for the marker.
            // This will be displayed on taping the marker
            markerOptions.title(latLng.latitude.toString() + " : " + latLng.longitude)
            // Clears the previously touched position
            googleMap.clear()

            // Animating to the touched position
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))

            // Placing a marker on the touched position
            googleMap.addMarker(markerOptions)

            val ubicacion = "Ubicación seleccionada"
            val latitud = latLng.latitude
            val longitud = latLng.longitude
            pasarCoordenadas(latitud, longitud)
            pasarUbicacion(ubicacion)
            findNavController().navigate(R.id.action_mapFragment_to_setPointFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMap2Binding.inflate(layoutInflater)
        cargaPuntos()
        createMap()
        return binding.root
    }

    private fun cargaPuntos() {
        db.collection("walls").addSnapshotListener(object: EventListener<QuerySnapshot> {
            override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                if(error != null){
                    Log.e("Firestore error", error.message.toString())
                    return
                }
                for(dc: DocumentChange in value?.documentChanges!!){
                    if(dc.type == DocumentChange.Type.ADDED){
                        val marker = (Wall(
                            dc.document.get("url") as String,
                            dc.document.get("nombre") as String,
                            dc.document.get("artista") as String,
                            dc.document.get("latitud") as Double,
                            dc.document.get("longitud") as Double
                        ))
                        markersList.add(marker)
                    }
                }
            }
        })
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun enableLocation() {
        if (!::map.isInitialized) return
        if (isLocationPermissionGranted()) {
            map.isMyLocationEnabled = true
            //Handler().postDelayed({posicionActual()},4000)
        } else {
            requestLocationPermission()
        }
    }

    val REQUEST_CODE_LOCATION = 100

    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            Toast.makeText(
                requireContext(),
                "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                map.isMyLocationEnabled = true

            } else {
                Toast.makeText(
                    requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (!::map.isInitialized) return
        if (!isLocationPermissionGranted()) {
            map.isMyLocationEnabled = false
            Toast.makeText(
                requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private lateinit var binding: FragmentMap2Binding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.addSite.setOnClickListener {

            val ubicacion = "Ubicación actual"

            var loc = map.myLocation
            val latitud = loc.latitude
            val longitud = loc.longitude
            pasarCoordenadas(latitud, longitud)
            pasarUbicacion(ubicacion)
            findNavController().navigate(R.id.action_mapFragment_to_setPointFragment)
        }
    }

    private fun pasarUbicacion(ubicacion: String) {
        parentFragmentManager.setFragmentResult("ubicacion", bundleOf("ubicacion" to ubicacion))
    }

    private fun pasarCoordenadas(latitud: Double, longitud: Double) {
        parentFragmentManager.setFragmentResult("longitud", bundleOf("longitud" to longitud))
        parentFragmentManager.setFragmentResult("latitud", bundleOf("latitud" to latitud))
    }
}