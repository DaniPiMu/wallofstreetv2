package com.example.wallofstreetv2.Fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.wallofstreetv2.MainActivity2
import com.example.wallofstreetv2.R
import com.google.firebase.auth.FirebaseAuth


class LogOutFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseAuth.getInstance().signOut()
        val intent = Intent(context, MainActivity2::class.java)
        startActivity(intent)
    }
}