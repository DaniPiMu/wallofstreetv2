package com.example.wallofstreetv2.Fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.wallofstreetv2.R
import com.example.wallofstreetv2.databinding.FragmentSignInBinding
import com.google.firebase.auth.FirebaseAuth

class SignInFragment : Fragment() {

    private lateinit var binding: FragmentSignInBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSignInBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnSignIn.setOnClickListener {
            if (binding.registerEmail.text.isNotEmpty() && binding.registerName.text.isNotEmpty() && binding.registerPassword.text.isNotEmpty())
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                    binding.registerEmail.text.toString(),
                    binding.registerPassword.text.toString()
                ).addOnCompleteListener {
                    if (it.isSuccessful) {
                        doneIt()
                    } else {
                        showAlert()
                    }
                }
        }
        binding.btnLinkToLogIn.setOnClickListener {
            val fm = parentFragmentManager
            val transaction = fm.beginTransaction()
            transaction.replace(R.id.fragmentContainerView1, LoginFragment())
            transaction.commit()
        }
    }

    private fun doneIt() {
        val fm = parentFragmentManager
        val transaction = fm.beginTransaction()
        transaction.replace(R.id.fragmentContainerView1, LoginFragment())
        transaction.commit()
    }

    private fun showAlert() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Error")
        builder.setMessage("Se ha producido un error de autenticando al usuario")
        builder.setPositiveButton("aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}