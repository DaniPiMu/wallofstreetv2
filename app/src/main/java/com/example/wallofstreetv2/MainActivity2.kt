package com.example.wallofstreetv2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.wallofstreetv2.Fragments.LoginFragment
import com.example.wallofstreetv2.databinding.ActivityMain2Binding

class MainActivity2 : AppCompatActivity() {

    private lateinit var binding: ActivityMain2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)
        binding = ActivityMain2Binding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()
        supportFragmentManager.beginTransaction().apply {
            replace(binding.fragmentContainerView1.id, LoginFragment())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }
    }
}