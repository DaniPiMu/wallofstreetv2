package com.example.wallofstreetv2

interface OnClickListener {
    fun onClick(wall: Wall)
}
