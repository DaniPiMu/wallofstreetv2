package com.example.wallofstreetv2

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Wall(
    var url: String ,
    var name: String,
    var artista: String,
    var latitud: Double,
    var longitud: Double
) : Parcelable

